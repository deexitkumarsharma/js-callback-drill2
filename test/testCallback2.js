const callback2 = require("../callback2");

function cb(err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
}

callback2("mcu453ed", cb);
