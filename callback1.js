/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is 
    passed from the given list of boards in boards.json and 
    then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");
let boards = require("./data/boards.json");

let callback1 = (boardId, callback) => {
  setTimeout(() => {
    fs.readFile("data/boards.json", "utf-8", (err, data) => {
      if (err) {
        throw err;
      } else {
        let parseData = JSON.parse(data);
        let res = parseData.find((ele) => ele.id === boardId);
        if (res) {
          callback(null, res);
        } else {
          callback(new Error("Board not found", res));
        }
      }
    });
  }, 2000);
};

//2nd
// let callback1 = (id, callback) => {
//   setTimeout(() => {
//     for (let index = 0; index < boards.length; index++) {
//       for (let boardsKey in boards[index]) {
//         if (boards[index][boardsKey] === id) {
//           result = boards[index];
//           if (result) {
//             callback(null, result);
//           } else {
//             callback(new Error("Board not found", result));
//           }
//         }
//       }
//     }
//   }, 2000);
// };

module.exports = callback1;
