/* 
	Problem 5: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const list = require("./callback2");
const card = require("./callback3");
const fs = require("fs");

let callback5 = (name, mindstone, spacestone) => {
  setTimeout(() => {
    fs.readFile("data/boards.json", "utf-8", function (error, data) {
      if (error) {
        console.log("File not found: ", error);
      }
      let parseData = JSON.parse(data);
      let dataOfThanos = parseData.filter((ele) => ele.name === name);
      let thanosId = dataOfThanos[0].id;
      console.log(dataOfThanos);

      list(thanosId, function (error, data) {
        if (error) {
          console.log(error);
        }
        console.log(data);

        let mindstoneid = data.filter((ele) => ele.name === mindstone)[0].id;
        let spacestoneid = data.filter((ele) => ele.name === spacestone)[0].id;

        card(mindstoneid, function (error, data) {
          if (error) {
            console.error("stone1 id error: ", error);
          }
          console.log("Mind details", data);
        });
        card(spacestoneid, function (error, data) {
          if (error) {
            console.error("stone2 id not get:", error);
          }
          console.log("Space details", data);
        });
      });
    });
  }, 2000);
};
module.exports = callback5;
