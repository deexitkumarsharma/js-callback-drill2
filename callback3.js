/* 
	Problem 3: Write a function that will return all cards that belong to a particular list 
    based on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

// 1st
const callback3 = (id, cb) => {
  setTimeout(() => {
    fs.readFile("data/cards.json", "utf-8", (err, data) => {
      if (err) {
        throw err;
      } else {
        let parseData = JSON.parse(data);
        let result = [];
        if (id in parseData) {
          result = parseData[id];
        } else {
          result = [];
        }
        cb(err, result);
      }
    });
  }, 2000);
};

//2nd
// const cards = require(`./data/cards.json`);

// let callback3 = (id, cb) => {
//   setTimeout(function () {
//     return cb(cards[id]);
//   }, 2000);
// };

module.exports = callback3;
