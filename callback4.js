/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const list = require("./callback2");
const card = require("./callback3");
const fs = require("fs");

let callback4 = (name) => {
  setTimeout(() => {
    fs.readFile("data/boards.json", "utf-8", function (error, data) {
      if (error) {
        console.log("file error occurred", error);
      }
      const parseData = JSON.parse(data);
      const dataOfThanos = parseData.filter((ele) => ele.name === name);
      const thanosId = dataOfThanos[0].id;
      console.log(dataOfThanos);

      list(thanosId, function (error, data) {
        if (error) {
          console.log("Thonos ID err occurred: ", error);
        }
        console.log(data);

        const mindId = data.filter((ele) => ele.name === "Mind")[0].id;

        card(mindId, function (error, data) {
          if (error) {
            console.error("Mind ID err occurred: ", error);
          }
          console.log(data);
        });
      });
    });
  }, 2000);
};

module.exports = callback4;
