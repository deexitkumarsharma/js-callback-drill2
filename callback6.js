/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const listFunc = require("./callback2");
const cardFunc = require("./callback3");
const fs = require("fs");

function callback6(name) {
  setTimeout(() => {
    fs.readFile("data/boards.json", "utf-8", function (error, data) {
      if (error) {
        console.error("file error occurred", error);
      }
      let boardData = JSON.parse(data);
      let thanosData = boardData.filter((ele) => ele.name == name);
      console.log(thanosData);

      let thanosId = thanosData[0].id;
      listFunc(thanosId, function (error, data) {
        if (error) {
          console.error("Thanos Id err occurred: ", error);
        }
        console.log(data);
        let arr = [];
        data.forEach((ele) => {
          arr.push(ele.id);
        });
        arr.forEach((ele) => {
          cardFunc(ele, function (error, data) {
            if (error) {
              console.error("List Id err: ", error);
            }
            console.log(ele, data);
          });
        });
      });
    });
  }, 2000);
}

module.exports = callback6;
