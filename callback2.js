/* 
	Problem 2: Write a function that will return all lists that belong to a board 
    based on the boardID that is passed to it from the given data in lists.json.
    Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

//1st Approach

// let callback2 = (boardId, list, callback) => {
//   setTimeout(() => {
//     let arr = Object.keys(list);
//     for (let i in arr) {
//       if (boardId == arr[i]) {
//         let result = list[arr[i]];
//         // console.log(result);
//         if (result) {
//           callback(null, result);
//         } else {
//           callback(new Error("Board not found", result));
//         }
//       }
//     }
//   }, 2000);
// };

// 2nd approach
const callback2 = (id, cb) => {
  setTimeout(() => {
    fs.readFile("data/lists.json", "utf-8", (err, data) => {
      if (err) {
        throw err;
      } else {
        let parseData = JSON.parse(data);
        let result;
        if (id in parseData) {
          result = parseData[id];
        } else {
          result = [];
        }
        cb(err, result);
      }
    });
  }, 2000);
};

//3rd short logic

// const lists = require(`./data/lists.json`)

// let callback2 = (id, cb) => {
//     setTimeout(function () {

//     return cb(lists[id]);
//     }, 2 * 1000);
// }

module.exports = callback2;
